define([
	'require',
	'madjoh_modules/wording/wording'
],
function(require, Wording){
	var TimeComparator = {
		weekDaysKeys 	: ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'],
		monthsKeys 		: ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
		dateOrderKeys : {
			'1' 	: 'first',
			'2' 	: 'second',
			'3' 	: 'third',
			'21' 	: 'sthg_first',
			'22' 	: 'sthg_second',
			'23' 	: 'sthg_third',
			'31' 	: 'sthg_first',
			other 	: 'nth'
		},
		
		parseDate : function(string, absolute){
			if(absolute){
				var times = string.split('T')[0].split('-');
				var date =  new Date();
					date.setYear(times[0]);
					date.setMonth(times[1]-1);
					date.setDate(times[2]);
					date.setHours(0);
					date.setMinutes(0);
					date.setSeconds(0);
				return date;
			}
			
			var times = string.split('+');
			var date = new Date(times[0]);

			if(times[1]){
				var hours = parseInt(times[1].substr(0,2));
				var minutes = parseInt(times[1].substr(2,2));
				date.setTime(date.getTime() - 1000*60*(minutes + 60*hours));
			}
			
			return date;
		},

		// DATE FORMAT
			dateFormat : {
				std : function(date){
					date = TimeComparator.getDate(date);
					return date.yyyy + '-' + date.mm + '-' + date.dd;
				},
				lang : function(date){
					return Wording.getText('date_format', TimeComparator.getDate(date));
				},
				explicit : function(date){
					date = TimeComparator.getDate(date, true);
						var dateOrderKey = TimeComparator.dateOrderKeys[date.dd] || TimeComparator.dateOrderKeys.other;
						var day 	= Wording.getText(dateOrderKey, 								{day 	: date.dd});
						var month 	= Wording.getText(TimeComparator.monthsKeys[date.mm - 1], 		{month 	: date.mm});
					return Wording.getText('explicit_date', {day : day, month : month});
				}
			},
			getDate : function(date, dynamicChars){
				var month = date.getMonth() + 1;
				if(month < 10 && !dynamicChars) month = '0' + month;

				var day = date.getDate();
				if(day < 10 && !dynamicChars) day = '0' + day;

				return {dd : day, mm : month, yyyy : date.getFullYear()};
			},

		// TIME AGO
			timeBlocks : [
				{name : 'year', max : null},
				{name : 'day', 	max : 365},
				{name : 'hour', max : 24},
				{name : 'min', 	max : 60},
				{name : 'sec', 	max : 60},
				{name : 'ms', 	max : 1000}
			],
			texts : [
				[
					'time_initials_years',
					'time_initials_days',
					'time_initials_hours',
					'time_initials_minutes',
					'time_initials_secondes',
					'just_now'
				], [
					'time_initials_years_days',
					'time_initials_days_hours',
					'time_initials_hours_minutes',
					'time_initials_minutes_seconds',
					'just_now',
					'just_now'
				]
			],
			shortTexts : [
				'time_short_initials_years',
				'time_short_initials_days',
				'time_short_initials_hours',
				'time_short_initials_minutes',
				'time_short_initials_secondes',
				'time_short_initials_secondes'
			],
			getRoundedTimeString : function(diff, decimals, isShort){
				if(!diff) return '';

				var i = 0;
				while(i < TimeComparator.timeBlocks.length && diff[TimeComparator.timeBlocks[i].name] === 0) i++;
				if(i === TimeComparator.timeBlocks.length) i = TimeComparator.timeBlocks.length - 1;

				if(i + decimals + 1 < TimeComparator.timeBlocks.length){
					if(diff[TimeComparator.timeBlocks[i + decimals + 1].name] > TimeComparator.timeBlocks[i + decimals + 1].max / 2){
						diff[TimeComparator.timeBlocks[i + decimals].name]++;

						while(decimals > 0 && diff[TimeComparator.timeBlocks[i + decimals].name] === TimeComparator.timeBlocks[i + decimals].max){
							diff[TimeComparator.timeBlocks[i + decimals].name] = 0;
							decimals--;
							diff[TimeComparator.timeBlocks[i + decimals].name]++;
						}
					}
				}

				if(isShort) return Wording.getText(TimeComparator.shortTexts[i], diff);
				return Wording.getText(TimeComparator.texts[decimals][i], diff);
			},
		
		// DATE DIFF
			dateDiff : function(date1, date2){
				var time_delay = localStorage.getItem('com.madjoh.time_delay');
				if(!time_delay) time_delay = 0;
				var pastTime = date2 - date1 - time_delay;
				var diff = TimeComparator.dateDiffParam(pastTime);
				return diff;
			},
			dateDiffPlus : function(date1, date2, millis){
				var time_delay = localStorage.getItem('com.madjoh.time_delay');
				if(!time_delay) time_delay = 0;

				// Time spent since date2 until date1.
				var pastTime = date1 - date2 - time_delay;

				// Time left before the millis deadline.
				var leftTime = millis - pastTime;

				var diff;
				if(leftTime > 0) diff = TimeComparator.dateDiffParam(leftTime);
				else diff = false;

				return diff;
			},
			dateDiffParam : function(tmp){
				var diff = {}							// Initialisation du retour
				
				diff.ms = tmp % 1000;					// Extraction du nombre de milisecondes

				tmp = Math.floor((tmp - diff.ms)/1000);	// Nombre de secondes entre les 2 dates
				diff.sec = tmp % 60;					// Extraction du nombre de secondes
			 
				tmp = Math.floor((tmp-diff.sec)/60);	// Nombre de minutes (partie entière)
				diff.min = tmp % 60;					// Extraction du nombre de minutes
			 
				tmp = Math.floor((tmp-diff.min)/60);	// Nombre d'heures (entières)
				diff.hour = tmp % 24;					// Extraction du nombre d'heures
				 
				tmp = Math.floor((tmp-diff.hour)/24);	// Nombre de jours restants
				diff.day = tmp % 365;

				tmp = Math.floor((tmp-diff.day)/365);	// Nombre d'années restantes
				diff.year = tmp;
				 
				return diff;
			},
			dateDiffString : function(date1, date2, weekdays, longFormat){
				var decimals 	= longFormat ? 1 : 0;
				var diff 		= TimeComparator.dateDiff(date1, date2);

					 if(diff.day > 6 || (!weekdays && diff.day > 1)) 	return TimeComparator.dateFormat[longFormat ? 'explicit' : 'lang'](date1);
				else if(diff.day > 1) 									return Wording.getText(TimeComparator.weekDaysKeys[date1.getDay()]);
				else if(diff.day === 1) 								return Wording.getText('yesterday');
				else 													return TimeComparator.getRoundedTimeString(diff, decimals);
			},

		// TIME AGO
			timeAgo 		: function(date 			){return TimeComparator.dateDiff 	  (date, new Date());},
			timeAgoString 	: function(date, longFormat ){return TimeComparator.dateDiffString(date, new Date(), false, longFormat);},
			expireString 	: function(dateStart, time, longFormat, isShort){
				var decimals 	= longFormat ? 1 : 0;
				var diff 		= TimeComparator.dateDiffPlus(new Date(), dateStart, time);
				return TimeComparator.getRoundedTimeString(diff, decimals, isShort);
			},
			timeLeft : function(dateStart, time, longFormat){
				var time = TimeComparator.expireString(dateStart, time, longFormat);
				return Wording.getText('time_left', {time : time});
			},

		// AGE
			getAge : function(birthdate){
				var now = new Date();
				var celebrated = -1;
				if(now.getMonth() > birthdate.getMonth() || (now.getMonth() === birthdate.getMonth() && now.getDate() >= birthdate.getDate())) celebrated = 0;

				return now.getFullYear() - birthdate.getFullYear() + celebrated;
			},
			getAgeString : function(birthdate){
				var age = TimeComparator.getAge(birthdate);
				return Wording.getText('age', {age : age});
			}
	};

	return TimeComparator;
});